<?php
$European_countries = array(array('country' => 'Unreal', 'capital' => 'Unreal'), array('country' => 'Albania', 'capital' => 'Tirana'),array('country' => 'Andorra', 'capital' => 'Andorra la Vella'),array('country' => 'Armenia', 'capital' => 'Yerevan'),array('country' => 'Austria', 'capital' => 'Vienna'),array('country' => 'Azerbaijan', 'capital' => 'Baku'),array('country' => 'Belarus', 'capital' => 'Minsk'),array('country' => 'Belgium', 'capital' => 'Brussels'),array('country' => 'Bosnia & Herzegovina', 'capital' => 'Sarajevo'),array('country' => 'Bulgaria', 'capital' => 'Sofia'),array('country' => 'Croatia', 'capital' => 'Zagreb'),array('country' => 'Czech Republic', 'capital' => 'Prague'),array('country' => 'Denmark', 'capital' => 'Copenhagen'),array('country' => 'Estonia', 'capital' => 'Tallinn'),array('country' => 'Finland', 'capital' => 'Helsinki'),array('country' => 'France', 'capital' => 'Paris'),array('country' => 'Georgia', 'capital' => 'Tbilisi'),array('country' => 'Germany', 'capital' => 'Berlin'),array('country' => 'Greece', 'capital' => 'Athens'),array('country' => 'Hungary', 'capital' => 'Budapest'),array('country' => 'Iceland', 'capital' => 'Reykjavik'),array('country' => 'Ireland', 'capital' => 'Dublin'),array('country' => 'Italy', 'capital' => 'Rome'),array('country' => 'Latvia', 'capital' => 'Riga'),array('country' => 'Liechtenstein', 'capital' => 'Vaduz'),array('country' => 'Lithuania', 'capital' => 'Vilnius'),array('country' => 'Luxembourg', 'capital' => 'Luxembourg'),array('country' => 'Macedonia', 'capital' => 'Skopje'),array('country' => 'Malta', 'capital' => 'Valletta'),array('country' => 'Moldova', 'capital' => 'Chisinau'),array('country' => 'Monaco', 'capital' => 'Monaco-Ville'),array('country' => 'Montenegro', 'capital' => 'Podgorica'),array('country' => 'The Netherlands', 'capital' => 'Amsterdam'),array('country' => 'Norway', 'capital' => 'Oslo'),array('country' => 'Poland', 'capital' => 'Warsaw'),array('country' => 'Portugal', 'capital' => 'Lisbon'),array('country' => 'Romania', 'capital' => 'Bucharest'),array('country' => 'Russia', 'capital' => 'Moscow'),array('country' => 'Serbia', 'capital' => 'Belgrade'),array('country' => 'Slovakia', 'capital' => 'Bratislava'),array('country' => 'Slovenia', 'capital' => 'Ljubljana'),array('country' => 'Spain', 'capital' => 'Madrid'),array('country' => 'Sweden', 'capital' => 'Stockholm'),array('country' => 'Turkey', 'capital' => 'Ankara'),array('country' => 'Ukraine', 'capital' => 'Kiev'),array('country' => 'United Kingdom', 'capital' => 'London'));

$link = 'http://api.openweathermap.org/data/2.5/weather';
$appid = '6ad177a43e99f2dc847c3d43c74f5b8b';
for($i = 0; $i < count($European_countries); $i++){
    $url=$link.'?q='.$European_countries[$i]['capital'].'&units=metric&appid='.$appid;
    $json = file_get_contents($url);  
    $obj = json_decode($json);
    if ($json === false || $obj === null){
        $European_countries[$i]['temperature'] = $European_countries[$i]['description'] = '<span style="color: #25a6f7">Не удалось получить данные</span>';
         $European_countries[$i]['weather_img'] = '<i style="font-size:24px; color: #25a6f7" class=\'fas fa-frown\'></i>';
     }  else {  
        $European_countries[$i]['temperature'] = (!empty($obj->main->temp)) ? round($obj->main->temp).' C&#176' : 'Не удалось получить данные'; 
        if(isset($obj->weather[0])){
            $European_countries[$i]['description'] = (!empty($obj->weather[0]->description)) ? $obj->weather[0]->description : 'Не удалось получить данные'; 
            $European_countries[$i]['weather_img'] = (!empty($obj->weather[0]->icon)) ? '<img style="width:50px; height:50px;" src="http://openweathermap.org/img/w/'.$obj->weather[0]->icon.'.png">': '<i style="font-size:24px; color: #25a6f7" class=\'fas fa-frown\'></i>';
        }
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    </head>
    <body> 
    <h1 style="width: 800px; margin: 50px auto 40px; text-align:center">Weather in European capitals</h1>
   <table class="table" style="width: 800px; margin:auto">
    <thead>
     <tr>
      <th scope="col">Country</th>
      <th scope="col">Capital</th>
      <th scope="col">Temperature</th>
      <th scope="col">Weather description</th>
      <th scope="col"></th>
     </tr>
   </thead>
    <?php
        foreach($European_countries as $European_country){
            echo '<tr>';
            echo '<td scope="col">'.$European_country['country'] .'</td>';
            echo '<td scope="col">'.$European_country['capital'].'</td>';
            echo '<td scope="col">'.$European_country['temperature'].' </td>';
            echo '<td scope="col">'.$European_country['description'].' </td>';
            echo '<td style="text-align: center; vertical-align: middle;" scope="col">'.$European_country['weather_img'].'</td>';
            echo '</tr>';
        }
    ?>
    </table>
    </body>
</html>    
	


